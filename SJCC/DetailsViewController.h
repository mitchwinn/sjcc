//
//  SJCCDetailsViewController.h
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DetailsViewController : UIViewController <UIWebViewDelegate> {
	NSDictionary* item;
	
	IBOutlet UIWebView* webView;
	IBOutlet UIActivityIndicatorView* loader;
}

@property (retain, nonatomic) NSDictionary* item;

@end