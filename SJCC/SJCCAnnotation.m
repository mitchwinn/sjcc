//
//  SJCCAnnotation.m
//  SJCC
//
//  Created by Mitch Winn on 12/11/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import "SJCCAnnotation.h"

@implementation SJCCAnnotation
@synthesize coordinate;

-(id)initWithCoordinate:(CLLocationCoordinate2D)c
{
    if (self = [super init]) {
        coordinate = c;
    }
    return self;
}

@end
