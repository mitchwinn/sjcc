//
//  SJCCContactViewController.m
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//


#import "SJCCContactViewController.h"

@implementation SJCCContactViewController

@synthesize nameTextField = _nameTextField;
@synthesize emailTextField = _emailTextField;
@synthesize addressTextField = _addressTextField;
@synthesize contactTextField = _contactTextField;
@synthesize otherTextField = _otherTextField;
@synthesize navBar = _navBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    UIImage *toolBarImage = [UIImage imageNamed:@"UIToolBar"];
    [[UINavigationBar appearance] setBackgroundImage:toolBarImage forBarMetrics:UIBarMetricsDefault];
}

- (void)viewDidLoad
{
    UIImage *buttonBack30 = [UIImage
                             imageNamed:@"UINavBarButton"];
    UIImage *buttonBack24 = [UIImage
                             imageNamed:@"UINavBarButton"];
    [[UIBarButtonItem appearance]
     setBackButtonBackgroundImage:buttonBack30
     forState:UIControlStateNormal
     barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance]
     setBackButtonBackgroundImage:buttonBack24
     forState:UIControlStateNormal
     barMetrics:UIBarMetricsLandscapePhone];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)submitButtonPressed:(id)sender
{
    if (_nameTextField.text == nil || _nameTextField.text.length < 1) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Oops!"
                                  message:@"Sorry, you need to have something in the name field."
                                  delegate:nil
                                  cancelButtonTitle:@"Done"
                                  otherButtonTitles:nil];
        
        [alertView show];
    }
    else {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"andy@stjohnscov.com", nil];
            [mailer setToRecipients:toRecipients];
            [mailer setSubject:@"New Contact"];
            
            NSString *emailBody = [NSString stringWithFormat:@"Name: %@ Email: %@ Address: %@ Preferred Method of Contact? %@ Anything Else? %@", [_nameTextField text], [_emailTextField text], [_addressTextField text], [_contactTextField text],
                                   [_otherTextField text]];
            
            [mailer setMessageBody:emailBody isHTML:NO];
            
            [self presentViewController:mailer animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        NSString *emailBody = [NSString stringWithFormat:@"Name: %@ Email: %@ Address: %@ Preferred Method of Contact %@ Anything Else? %@", [_nameTextField text], [_emailTextField text], [_addressTextField text], [_contactTextField text], [_otherTextField text]];
        NSLog(@"%@", emailBody);
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)nameFieldDoneEditing:(id)sender {
    [_nameTextField resignFirstResponder];
}
- (IBAction)emailFieldDoneEditing:(id)sender {
    [_emailTextField resignFirstResponder];
}
- (IBAction)addressFieldDoneEditing:(id)sender {
    [_addressTextField resignFirstResponder];
}

- (IBAction)contactFieldDoneEditing:(id)sender {
    [_contactTextField resignFirstResponder];
}
- (IBAction)otherFieldDoneEditing:(id)sender {
    [_otherTextField resignFirstResponder];
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidUnload {
    [self setNavBar:nil];
    [super viewDidUnload];
}
@end
