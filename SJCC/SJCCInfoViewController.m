//
//  SJCCInfoViewController.m
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import "SJCCInfoViewController.h"
#import "SJCCAnnotation.h"

@implementation SJCCInfoViewController

@synthesize mapView = _mapView;

-(void)viewWillAppear:(BOOL)animated
{
    UIImage *navImage = [UIImage imageNamed:@"UINavBar"];
    [[UINavigationBar appearance] setBackgroundImage:navImage forBarMetrics:UIBarMetricsDefault];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    UIImage *barImage = [UIImage imageNamed:@"UINavBar"];
    [[UINavigationBar appearance] setBackgroundImage:barImage forBarMetrics:UIBarMetricsDefault];
    
    [super viewDidLoad];
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 45.593852;
    coordinate.longitude = -122.730077;
    
    [_mapView addAnnotation:[[SJCCAnnotation alloc]
                             initWithCoordinate:coordinate]];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5 * METERS_PER_MILE, 0.5 * METERS_PER_MILE);
   
    MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
    
    [_mapView setRegion:adjustedRegion animated:YES];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:5035286628"]];
    }
}

- (IBAction)directionsButtonPressed:(id)sender {
    NSLog(@"directions button pressed");
    
    NSString *reqSysVer = @"6.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        NSString* address = @"6265 N. Columbia Way, Portland, 97203";
        //http://maps.apple.com/maps?daddr=San+Francisco,+CA&saddr=cupertino
        NSString* url = [NSString stringWithFormat: @"http://maps.apple.com/maps?daddr=%@",
                         [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    }
    else {
        NSString* address = @"6265 N. Columbia Way, Portland, 97203";
        NSString* url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=Current+Location&daddr=%@",
                         [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    }
    
}

- (IBAction)phoneButtonPressed:(id)sender {
    NSLog(@"phone button pressed");
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Are You Sure?!"
                              message:@"Do you want to call the church?"
                              delegate:self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes",nil];
    
    [alertView show];
    
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end

