//
//  SJCCInfoViewController.h
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#define METERS_PER_MILE 1609.344

@interface SJCCInfoViewController : UIViewController

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end