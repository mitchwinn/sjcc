//
//  SJCCTableViewController.h
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSLoader.h"
#import "DetailsViewController.h"

@interface SJCCTableViewController : UITableViewController<RSSLoaderDelegate> {
    RSSLoader* rss;
	NSMutableArray* rssItems;
}

@end