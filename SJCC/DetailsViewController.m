//
//  SJCCDetailsViewController.m
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//
#import "DetailsViewController.h"

@implementation DetailsViewController

@synthesize item;

-(void)viewWillAppear:(BOOL)animated
{
    UIImage *navImage = [UIImage imageNamed:@"UINavBar"];
    [[UINavigationBar appearance] setBackgroundImage:navImage forBarMetrics:UIBarMetricsDefault];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [loader startAnimating];
    
    UIImage *buttonBack30 = [UIImage
                             imageNamed:@"UINavBarButton"];
    UIImage *buttonBack24 = [UIImage
                             imageNamed:@"UINavBarButton"];
    [[UIBarButtonItem appearance]
     setBackButtonBackgroundImage:buttonBack30
     forState:UIControlStateNormal
     barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance]
     setBackButtonBackgroundImage:buttonBack24
     forState:UIControlStateNormal
     barMetrics:UIBarMetricsLandscapePhone];
	
	webView.delegate = self;
	webView.scalesPageToFit = YES;
	NSURL* url = [NSURL URLWithString:[item objectForKey:@"link"]];
	[webView loadRequest:[NSURLRequest requestWithURL:url]];
	NSLog(@"loading url...");
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
	//very important, otherwise unfinished requests will cause exc_badaccess
	webView.delegate = nil;
}

#pragma mark -
#pragma mark UIWebViewDelegate messages
- (void)webViewDidStartLoad:(UIWebView *)webView
{
	loader.hidden = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [loader stopAnimating];
	loader.hidden = YES;
}


@end
