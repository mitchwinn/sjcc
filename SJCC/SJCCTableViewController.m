//
//  SJCCTableViewController.m
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//
#import "SJCCTableViewController.h"
#define NAVBACK @"UIImageCarbon"
#define CELLBACK @"UITableViewCell"

@implementation SJCCTableViewController



-(void)viewDidLoad
{
    [super viewDidLoad];
    
	rssItems = nil;
	rss = nil;
    self.navigationController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:NAVBACK]];
    [self.tableView setIndicatorStyle:UIScrollViewIndicatorStyleBlack];
    self.tableView.rowHeight = 97;
    self.tableView.backgroundColor = [UIColor clearColor];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    UIImage *navImage = [UIImage imageNamed:@"UINavBar"];
    [[UINavigationBar appearance] setBackgroundImage:navImage forBarMetrics:UIBarMetricsDefault];
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	
	if (rss==nil)
    {
		rss = [[RSSLoader alloc] init];
		rss.delegate = self;
		[rss load];
	}
}

// Customize the number of sections in the table view.
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// Customize the number of rows in the table view.
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (rss.loaded == YES)
    {
		return [rssItems count];
	} else {
		return 1;
	}
}

-(UITableViewCell *)getLoadingTableCellWithTableView:(UITableView *)tableView
{
    static NSString *LoadingCellIdentifier = @"LoadingCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:LoadingCellIdentifier];
    
	if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LoadingCellIdentifier];
        cell.backgroundView = [[UIImageView alloc] init];
        cell.selectedBackgroundView = [[UIImageView alloc] init];
    }
	
    UIImage *cellBackgroundImage = [UIImage imageNamed:CELLBACK];
    ((UIImageView *)cell.backgroundView).image = cellBackgroundImage;
    
    cell.textLabel.textColor = [UIColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
	cell.textLabel.text = @"Loading...";
	
	UIActivityIndicatorView* activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[activity startAnimating];
	[cell setAccessoryView: activity];
	
    return cell;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	if (rss.loaded == NO)
    {
		return [self getLoadingTableCellWithTableView:tableView];
	}
	
    static NSString *CellIdentifier = @"TitleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundView = [[UIImageView alloc] init];
        cell.selectedBackgroundView = [[UIImageView alloc] init];
		//cell.selectedBackgroundView = [[UIImageView alloc] init];
    }
    
    UIImage *cellBackgroundImage = [UIImage imageNamed:@"UITableViewCell"];
    ((UIImageView *)cell.backgroundView).image = cellBackgroundImage;
    
    UIImage *cellSelectedBackgroundImage = [UIImage imageNamed:@"UITableViewCellSelected"];
    ((UIImageView *)cell.selectedBackgroundView).image = cellSelectedBackgroundImage;
    
    NSDictionary* item = [rssItems objectAtIndex: indexPath.row];
    
    cell.textLabel.text = [item objectForKey:@"title"];
    cell.textLabel.textColor = [UIColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	//[tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    DetailsViewController *detailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailsViewController"];
	detailsViewController.item = [rssItems objectAtIndex:floor(indexPath.row)];
	[self.navigationController pushViewController:detailsViewController animated:YES];
}

-(void)dealloc
{
	rssItems = nil;
	rss = nil;
}

-(void)updatedFeedWithRSS:(NSMutableArray*)items
{
	rssItems = items;
	[self.tableView reloadData];
}

-(void)failedFeedUpdateWithError:(NSError *)error
{
	NSLog(@"Error fetching feed...");
}

-(void)updatedFeedTitle:(NSString*)rssTitle
{
    //[(TableHeaderView*)self.tableView.tableHeaderView setText:rssTitle];
}

@end
