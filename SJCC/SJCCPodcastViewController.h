//
//  SJCCPodcastViewController.h
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SJCCPodcastViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIActivityIndicatorView* loader;
}

@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end