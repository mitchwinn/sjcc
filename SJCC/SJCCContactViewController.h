//
//  SJCCContactViewController.h
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SJCCContactViewController : UIViewController <MFMailComposeViewControllerDelegate>

- (IBAction)submitButtonPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UITextField *contactTextField;
@property (strong, nonatomic) IBOutlet UITextField *otherTextField;
@property (strong, nonatomic) IBOutlet UINavigationItem *navBar;

@end