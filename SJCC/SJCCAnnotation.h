//
//  SJCCAnnotation.h
//  SJCC
//
//  Created by Mitch Winn on 12/11/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SJCCAnnotation : NSObject <MKAnnotation>
{
    NSString * title;
    NSString * subtitle;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
-(id)initWithCoordinate:(CLLocationCoordinate2D)c;

@end
