//
//  SJCCPodcastViewController.m
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import "SJCCPodcastViewController.h"
#define WEB_SITE @"http://stjohnscov.com/home/sermons/?wpmp_switcher=mobile"

@implementation SJCCPodcastViewController

@synthesize webView = _webView;

-(void)viewWillAppear:(BOOL)animated
{
    UIImage *navImage = [UIImage imageNamed:@"UINavBar"];
    [[UINavigationBar appearance] setBackgroundImage:navImage forBarMetrics:UIBarMetricsDefault];
}



/***********************************************
 *
 *  viewDidLoad
 *
 *  return: void
 *
 *  purpose: load webpage and display alert if
 *           this is the users first time
 *           opening the app.
 *
 ***********************************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [loader startAnimating];
    
    NSString *urlString = WEB_SITE;
    NSURL *url = [NSURL URLWithString:urlString];
    _webView.delegate = self;
	_webView.scalesPageToFit = YES;
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
    
    BOOL didRunBefore = [[NSUserDefaults standardUserDefaults] boolForKey:@"didRunBefore"];
    
    if (!didRunBefore) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Podcast"
                                                          message:@"Click the link url under each player to begin listening"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        // show alert;
        [message show];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"didRunBefore"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
/***********************************************
 *
 *  reloadButton
 *
 *  return: action
 *
 *  purpose: reloads the webpage when button is
 *            pressed.
 *
 ***********************************************/
- (IBAction)reloadButton:(id)sender {
    [_webView reload];
}

- (void)dealloc {
	//very important, otherwise unfinished requests will cause exc_badaccess
	self.webView.delegate = nil;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	loader.hidden = NO;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [loader stopAnimating];
	loader.hidden = YES;
}

@end

