//
//  SJCCSundayViewController.h
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <dispatch/dispatch.h>

@interface SJCCSundayViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    dispatch_queue_t backgroundQueue;
}

- (IBAction)submitButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIView *prayerView;
@property (strong, nonatomic) IBOutlet UIWebView *sermonWebView;

@property (weak, nonatomic) IBOutlet UITextField *requestTextField;

@property (weak, nonatomic) IBOutlet UITextField *fromTextField;
@property (strong, nonatomic) IBOutlet UIToolbar *sundayToolBar;

@end