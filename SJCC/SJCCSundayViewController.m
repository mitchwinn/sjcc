//
//  SJCCSundayViewController.m
//  SJCC
//
//  Created by Mitch Winn on 12/10/12.
//  Copyright (c) 2012 Mitch Winn. All rights reserved.
//

#import "SJCCSundayViewController.h"
#define BULLETIN_URL @"http://stjohnscov.com/wp-content/uploads/SJCCApp/Bulletin.pdf"
#define SERMON_URL @"http://stjohnscov.com/wp-content/uploads/SJCCApp/Sermon.pdf"

enum SundaySegs {
    BULLETIN = 0,
    SERMON = 1,
    PRAYER = 2
};

@implementation SJCCSundayViewController
{
    BOOL alreadyLoadBulletin;
    BOOL alreadyLoadSermon;
    BOOL isBulletin;
    BOOL isSermon;
}
@synthesize webView = _webView;
@synthesize prayerView = _prayerView;
@synthesize sermonWebView = _sermonWebView;
@synthesize requestTextField;
@synthesize fromTextField;
@synthesize sundayToolBar;

- (id) init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    UIImage *navImage = [UIImage imageNamed:@"UINavBar"];
    [[UINavigationBar appearance] setBackgroundImage:navImage forBarMetrics:UIBarMetricsDefault];
}


-(void)loadDocument:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    backgroundQueue = dispatch_queue_create("com.mitchwinn.bgqueue", NULL);
    isSermon = FALSE;
    isBulletin = TRUE;
    [_webView setHidden:NO];
    [_sermonWebView setHidden:YES];
    [_prayerView setHidden:YES];
    
    // load pdf in the webview
    dispatch_async(backgroundQueue, ^(void){
        [self loadDocument:BULLETIN_URL];
    });
    //[self loadDocument];
    
    UIImage *toolBarImage = [UIImage imageNamed:@"UIToolBar"];
    [sundayToolBar setBackgroundImage:toolBarImage forToolbarPosition:0 barMetrics:0];
}

- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    switch (selectedSegment) {
        case BULLETIN:
            [_webView setHidden:NO];
            [_sermonWebView setHidden:YES];
            [_prayerView setHidden:YES];
            isSermon = FALSE;
            isBulletin = TRUE;
            if (alreadyLoadBulletin == FALSE) {
                // load pdf in the webview
                dispatch_async(backgroundQueue, ^(void){
                    [self loadDocument:BULLETIN_URL];
                });
                
                alreadyLoadBulletin = TRUE;
            }
            break;
        case SERMON:
            [_webView setHidden:YES];
            [_sermonWebView setHidden:NO];
            [_prayerView setHidden:YES];
            isSermon = TRUE;
            isBulletin = FALSE;
            if (alreadyLoadSermon == FALSE) {
                // load pdf in the webview
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self loadDocument:SERMON_URL];
                });
                alreadyLoadSermon = TRUE;
            }
            break;
        case PRAYER:
            [_webView setHidden:YES];
            [_sermonWebView setHidden:YES];
            [_prayerView setHidden:NO];
            break;
        default:
            break;
    }
}
- (IBAction)requestTextFieldDoneEditing:(id)sender {
    [requestTextField resignFirstResponder];
}

- (IBAction)fromTextFieldDoneEditing:(id)sender {
    [fromTextField resignFirstResponder];
}
- (IBAction)submitButtonTapped:(id)sender {
    UIImage *navImage = [UIImage imageNamed:@"UIToolBar"];
    [[UINavigationBar appearance] setBackgroundImage:navImage forBarMetrics:UIBarMetricsDefault];
    if (requestTextField.text == nil || requestTextField.text.length < 1) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Oops!"
                                  message:@"Sorry, you need to have something in the request field."
                                  delegate:nil
                                  cancelButtonTitle:@"Done"
                                  otherButtonTitles:nil];
        
        [alertView show];
    }
    else {
        if (fromTextField.text.length < 1) {
            fromTextField.text = @"Anonymous";
        }
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            [mailer setSubject:@"Prayer Request"];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"andy@stjohnscov.com", nil];
            [mailer setToRecipients:toRecipients];
            
            NSString *emailBody = [NSString stringWithFormat:@"%@ sent from %@", [requestTextField text], [fromTextField text]];;
            [mailer setMessageBody:emailBody isHTML:NO];
            
            [self presentViewController:mailer animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        NSString *prayerRequest = [NSString stringWithFormat:@"%@ sent from %@", [requestTextField text], [fromTextField text]];
        NSLog(@"%@", prayerRequest);
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)viewDidUnload
{
    [self setWebView:nil];
    [self setPrayerView:nil];
    [self setRequestTextField:nil];
    [self setFromTextField:nil];
    [self setSermonWebView:nil];
    [self setSundayToolBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

