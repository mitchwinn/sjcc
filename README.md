SJCC
====

St. John's Covenant Church iOS Application

About
=====

SJCC is an iOS Application for a local church located in Portland, Oregon. The application links to the church blog as well as the podcast.
